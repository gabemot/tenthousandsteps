package org.tenthousandsteps;

import org.junit.*;
import org.tenthousandsteps.exceptions.OutOfStockException;
import org.tenthousandsteps.stock.Coin;
import org.tenthousandsteps.stock.Snack;
import org.tenthousandsteps.util.Pair;
import org.tenthousandsteps.util.SnackMachineFactory;

import java.util.Arrays;
import java.util.List;

public class SnackMachineTest {
    private static SnackMachine snackMachine;

    @BeforeClass
    public static void setUp() {
        snackMachine = SnackMachineFactory.getSnackMachine();
    }

    @AfterClass
    public static void tearDown() {
        snackMachine = null;
    }

    @Test
    public void testBuySnackWithExactPrice() {
        // Select a snack
        int price = snackMachine.selectSnackAndGetPrice(Snack.EUGENIA);

        // The price returned by the method above should be equal
        // to the price stored in the enum.
        // If it's not, than 'selectSnackAndGetPrice' is doing something wrong.
        Assert.assertEquals(Snack.EUGENIA.getPrice(), price);

        // Insert 50 pennies into the machine
        snackMachine.insertCoin(Coin.FIFTY_PENNIES);

        Pair<Snack, List<Coin>> snackAndChange = snackMachine.collectSnackAndChange();
        Snack snack = snackAndChange.getFirst();
        List<Coin> change = snackAndChange.getSecond();

        // The snack I got should be EUGENIA...
        Assert.assertEquals(Snack.EUGENIA, snack);

        // ... and since I paid the exact amount,
        // there should be no change
        Assert.assertTrue(change.isEmpty());
    }

    @Test
    public void testBuySnackWithChange() {
        int price = snackMachine.selectSnackAndGetPrice(Snack.CRISPS);
        Assert.assertEquals(Snack.CRISPS.getPrice(), price);

        List<Coin> coinsInMyPocket = Arrays.asList(Coin.FIFTY_PENNIES, Coin.TWENTY_PENNIES, Coin.TEN_PENNIES);
        int totalAmountInsertedInTheMachine = 0;

        for (Coin coin : coinsInMyPocket) {
            snackMachine.insertCoin(coin);
            totalAmountInsertedInTheMachine += coin.getDenomination();
        }

        Pair<Snack, List<Coin>> snackAndChange = snackMachine.collectSnackAndChange();
        Snack snack = snackAndChange.getFirst();
        List<Coin> change = snackAndChange.getSecond();

        // The snack should be CRISPS
        Assert.assertEquals(Snack.CRISPS, snack);

        // There should be some change
        Assert.assertFalse(change.isEmpty());

        // Check that we got the right amount of change
        Assert.assertEquals(totalAmountInsertedInTheMachine - Snack.CRISPS.getPrice(), getTotal(change));
    }

    private int getTotal(List<Coin> coins) {
        int total = 0;
        for (Coin c : coins) {
            total += c.getDenomination();
        }
        return total;
    }

    @Test
    public void testRefund() {
        int price = snackMachine.selectSnackAndGetPrice(Snack.MARS);
        Assert.assertEquals(Snack.MARS.getPrice(), price);

        List<Coin> coinsInMyPocket = Arrays.asList(Coin.TWENTY_PENNIES, Coin.TWENTY_PENNIES, Coin.TEN_PENNIES);
        int totalAmountInsertedInTheMachine = 0;

        for (Coin coin : coinsInMyPocket) {
            snackMachine.insertCoin(coin);
            totalAmountInsertedInTheMachine += coin.getDenomination();
        }

        Assert.assertEquals(totalAmountInsertedInTheMachine, getTotal(snackMachine.refund()));
    }

    @Test(expected = OutOfStockException.class)
    public void testOutOfStock() {
        for (int i = 0; i < 11; i++) {
            snackMachine.selectSnackAndGetPrice(Snack.COKE);
            snackMachine.insertCoin(Coin.ONE_POUND);
            snackMachine.collectSnackAndChange();
        }
    }

    //    @Test(expected = InsufficientChangeException.class)
    @Ignore
    public void testInsufficientChangeException() {
        for (int i = 0; i < 10; i++) {
            snackMachine.selectSnackAndGetPrice(Snack.CRISPS);
            snackMachine.insertCoin(Coin.FIFTY_PENNIES);
            snackMachine.insertCoin(Coin.TWENTY_PENNIES);
            snackMachine.insertCoin(Coin.TEN_PENNIES);
            Pair<Snack, List<Coin>> crispsAndChange = snackMachine.collectSnackAndChange();
            System.out.println("crispsAndChange = " + crispsAndChange);

            snackMachine.selectSnackAndGetPrice(Snack.MARS);
            snackMachine.insertCoin(Coin.FIFTY_PENNIES);
            snackMachine.insertCoin(Coin.FIVE_PENNIES);
            snackMachine.insertCoin(Coin.TWENTY_PENNIES);
            Pair<Snack, List<Coin>> marsAndChange = snackMachine.collectSnackAndChange();
            System.out.println("marsAndChange = " + marsAndChange);

            snackMachine.selectSnackAndGetPrice(Snack.WATER);
            snackMachine.insertCoin(Coin.FIFTY_PENNIES);
            snackMachine.insertCoin(Coin.TWENTY_PENNIES);
            snackMachine.insertCoin(Coin.TWENTY_PENNIES);
            Pair<Snack, List<Coin>> waterAndChange = snackMachine.collectSnackAndChange();
            System.out.println("waterAndChange = " + waterAndChange);
        }
    }

    @Test(expected = OutOfStockException.class)
    public void testReset() {
        SnackMachine someOtherSnackMachine = SnackMachineFactory.getSnackMachine();
        someOtherSnackMachine.reset();
        someOtherSnackMachine.selectSnackAndGetPrice(Snack.WATER);
    }

    @Ignore
    public void testSnackMachineImpl() {
        SnackMachineImpl snackMachineImpl = new SnackMachineImpl();
    }
}
