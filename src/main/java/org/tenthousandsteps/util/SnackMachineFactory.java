package org.tenthousandsteps.util;

import org.tenthousandsteps.SnackMachine;
import org.tenthousandsteps.SnackMachineImpl;

public class SnackMachineFactory {

    private static SnackMachine snackMachine;

    private SnackMachineFactory() {
    }

    public static SnackMachine getSnackMachine() {
        if (snackMachine == null) {
            snackMachine = new SnackMachineImpl();
        }
        return snackMachine; // SnackMachine is a singleton in our application
    }

}
