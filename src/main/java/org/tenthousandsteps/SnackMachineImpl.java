package org.tenthousandsteps;

import org.tenthousandsteps.exceptions.InsufficientChangeException;
import org.tenthousandsteps.exceptions.NotFullPaidException;
import org.tenthousandsteps.exceptions.OutOfStockException;
import org.tenthousandsteps.stock.Coin;
import org.tenthousandsteps.stock.Inventory;
import org.tenthousandsteps.stock.Snack;
import org.tenthousandsteps.util.Pair;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SnackMachineImpl implements SnackMachine {

    private final Inventory<Coin> cashInventory = new Inventory<>();
    private final Inventory<Snack> snackInventory = new Inventory<>();
    private int totalSales;
    private Snack currentSnack;
    private int currentBalance;

    public SnackMachineImpl() {
        initialize();
    }

    private void initialize() {
        for (Snack snack : Snack.values()) {
            snackInventory.put(snack, 10);
        }

        for (Coin coin : Coin.values()) {
            cashInventory.put(coin, coin.equals(Coin.ONE_POUND) ? 10 : 20);
        }
    }

    @Override
    public int selectSnackAndGetPrice(Snack snack) {
        if (snackInventory.hasItem(snack)) {
            currentSnack = snack;
            return currentSnack.getPrice();
        } else {
            throw new OutOfStockException("This snack is no longer available. PLease choose another one.");
        }
    }

    @Override
    public void insertCoin(Coin coin) {
        currentBalance += coin.getDenomination();
        cashInventory.add(coin);
    }

    @Override
    public List<Coin> refund() {
        List<Coin> refund = getChange(currentBalance);
        updateCashInventory(refund);
        currentBalance = 0;
        currentSnack = null;
        return refund;
    }

    @Override
    public Pair<Snack, List<Coin>> collectSnackAndChange() {
        Snack snack = collectSnack();
        totalSales += currentSnack.getPrice();
        List<Coin> change = collectChange();
        return new Pair<>(snack, change);
    }

    private Snack collectSnack() {
        if (isFullPaid()) {
            if (hasSufficientChange()) {
                snackInventory.deduct(currentSnack);
                return currentSnack;
            } else {
                throw new InsufficientChangeException("Not enough funds to give your change. Please select another product.");
            }
        } else {
            int remainingBalance = currentSnack.getPrice() - currentBalance;
            throw new NotFullPaidException("Price not fully paid; remaining: ", remainingBalance);
        }
    }

    private List<Coin> collectChange() {
        int changeAmount = currentBalance - currentSnack.getPrice();
        List<Coin> change = getChange(changeAmount);
        updateCashInventory(change);
        currentBalance = 0;
        currentSnack = null;
        return change;
    }

    private void updateCashInventory(List<Coin> change) {
        for (Coin coin : change) {
            cashInventory.deduct(coin);
        }
    }

    private boolean isFullPaid() {
        return (currentBalance >= currentSnack.getPrice());
    }

    private boolean hasSufficientChange() {
        return hasSufficientChangeForAmount(currentBalance - currentSnack.getPrice());
    }

    private boolean hasSufficientChangeForAmount(int amount) {
        try {
            getChange(amount);
        } catch (InsufficientChangeException e) {
            return false;
        }
        return true;
    }

    private List<Coin> getChange(int amount) {
        List<Coin> changes = Collections.emptyList();

        if (amount > 0) {
            changes = new ArrayList<>(); // the list of coins to be returned to the customer as change
            int balance = amount;
            while (balance > 0) {
                if (balance >= Coin.ONE_POUND.getDenomination() && cashInventory.hasItem(Coin.ONE_POUND)) {
                    changes.add(Coin.ONE_POUND);
                    balance -= Coin.ONE_POUND.getDenomination();
                } else if (balance >= Coin.FIFTY_PENNIES.getDenomination() && cashInventory.hasItem(Coin.FIFTY_PENNIES)) {
                    changes.add(Coin.FIFTY_PENNIES);
                    balance -= Coin.FIFTY_PENNIES.getDenomination();
                } else if (balance >= Coin.TWENTY_PENNIES.getDenomination() && cashInventory.hasItem(Coin.TWENTY_PENNIES)) {
                    changes.add(Coin.TWENTY_PENNIES);
                    balance -= Coin.TWENTY_PENNIES.getDenomination();
                } else if (balance >= Coin.TEN_PENNIES.getDenomination() && cashInventory.hasItem(Coin.TEN_PENNIES)) {
                    changes.add(Coin.TEN_PENNIES);
                    balance -= Coin.TEN_PENNIES.getDenomination();
                } else if (balance >= Coin.FIVE_PENNIES.getDenomination() && cashInventory.hasItem(Coin.FIVE_PENNIES)) {
                    changes.add(Coin.FIVE_PENNIES);
                    balance -= Coin.FIVE_PENNIES.getDenomination();
                } else {
                    throw new InsufficientChangeException("Not enough funds to give your change. Please select another product.");
                }
            }
        }

        return changes;
    }

    @Override
    public void reset() {
        cashInventory.clear();
        snackInventory.clear();
        totalSales = 0;
        currentSnack = null;
        currentBalance = 0;
    }

    public void printStats() {
        System.out.println("Total Sales : " + totalSales);
        System.out.println("Current Snack Inventory : " + snackInventory);
        System.out.println("Current Cash Inventory : " + cashInventory);
    }

    public long getTotalSales() {
        return totalSales;
    }

    @Override
    public Inventory<Snack> getSnackInventory() {
        return snackInventory;
    }

    @Override
    public int getCurrentBalance() {
        return currentBalance;
    }
}
