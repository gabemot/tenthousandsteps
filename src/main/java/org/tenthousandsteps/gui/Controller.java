package org.tenthousandsteps.gui;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import org.tenthousandsteps.SnackMachine;
import org.tenthousandsteps.exceptions.OutOfStockException;
import org.tenthousandsteps.stock.Coin;
import org.tenthousandsteps.stock.Snack;
import org.tenthousandsteps.util.Pair;
import org.tenthousandsteps.util.SnackMachineFactory;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Controller {

    @FXML public TableView<RowData> tableView;
    @FXML public TableColumn<RowData, String> name;
    @FXML public TableColumn<RowData, Integer> price;
    @FXML public TableColumn<RowData, Integer> quantity;

    @FXML public Label itemPriceLabel;
    @FXML public Spinner<Integer> desiredQty;
    @FXML public Label totalPriceLabel;

    @FXML public VBox paymentContainer;
    @FXML public HBox coinsContainer;
    @FXML public ChoiceBox<Integer> coinsList;
    @FXML public Button insertCoinButton;
    @FXML public Label currentBalanceLabel;
    @FXML public Button checkoutButton;
    @FXML public Label statusLabel;

    private SnackMachine snackMachine;

    @FXML
    private void initialize() {
        System.out.println("Controller initialized.");
        snackMachine = SnackMachineFactory.getSnackMachine();
        loadColumns();
        List<RowData> tableRows = Arrays.stream(Snack.values()).map(snack -> new RowData(snack.name(), snack.getPrice(), snackMachine.getSnackInventory().getQuantity(snack))).collect(Collectors.toList());
        tableView.setItems(FXCollections.observableList(tableRows));

        List<Integer> coinValues = Arrays.stream(Coin.values()).map(Coin::getDenomination).collect(Collectors.toList());
        coinsList.setItems(FXCollections.observableList(coinValues));
    }

    private void loadColumns() {
        name.setCellValueFactory(new PropertyValueFactory<>("name"));
        price.setCellValueFactory(new PropertyValueFactory<>("price"));
        quantity.setCellValueFactory(new PropertyValueFactory<>("quantity"));
    }

    public void onSelection() {

        RowData selectedRow = tableView.getSelectionModel().getSelectedItem();
        try {
            int snackPrice = snackMachine.selectSnackAndGetPrice(Snack.valueOf(selectedRow.getName()));
            itemPriceLabel.setText("Price: " + snackPrice);
            totalPriceLabel.setText("Total: " + snackPrice * desiredQty.getValue());
        } catch (OutOfStockException e) {
            itemPriceLabel.setText("Price: " + e.getMessage());
            totalPriceLabel.setText("Total: " + e.getMessage());
        }
    }

    public void onBuy(ActionEvent actionEvent) {
        tableView.setDisable(true);
        itemPriceLabel.setDisable(true);
        desiredQty.setDisable(true);
        totalPriceLabel.setDisable(true);

        paymentContainer.setVisible(true);
    }

    public void onRefund(ActionEvent actionEvent) {
        tableView.setDisable(false);
        itemPriceLabel.setDisable(false);
        desiredQty.setDisable(false);
        totalPriceLabel.setDisable(false);

        paymentContainer.setVisible(false);
    }

    public void onCoinInsertion(ActionEvent actionEvent) {
        final Integer selectedCoinValue = coinsList.getSelectionModel().getSelectedItem();
        if (selectedCoinValue != null) {
            Arrays.stream(Coin.values()).filter(coin -> selectedCoinValue.equals(coin.getDenomination())).findFirst().ifPresent(coin -> snackMachine.insertCoin(coin));
            currentBalanceLabel.setText("Current balance: " + snackMachine.getCurrentBalance());
        }
    }

    public void collectSnack(ActionEvent actionEvent) {
        try {
            final Pair<Snack, List<Coin>> snackAndChange = snackMachine.collectSnackAndChange();
            statusLabel.setText("Here is your " + snackAndChange.getFirst().name() + " and " + getTotal(snackAndChange.getSecond()) + " change.");
        } catch (Exception e) {
            statusLabel.setText(e.getMessage());
        }
    }

    private int getTotal(List<Coin> coins) {
        return coins.stream().mapToInt(Coin::getDenomination).sum();
    }
}
