package org.tenthousandsteps.exceptions;

public class NotFullPaidException extends RuntimeException {

    private final String message;
    private final int remaining;

    public NotFullPaidException(String message, int remaining) {
        this.message = message;
        this.remaining = remaining;
    }

    public int getRemaining() {
        return remaining;
    }

    @Override
    public String getMessage() {
        return message + remaining;
    }

}
