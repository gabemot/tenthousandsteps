package org.tenthousandsteps.stock;

public enum Coin {

    FIVE_PENNIES(5),
    TEN_PENNIES(10),
    TWENTY_PENNIES(20),
    FIFTY_PENNIES(50),
    ONE_POUND(100);

    private final int denomination;

    Coin(int denomination) {
        this.denomination = denomination;
    }

    public int getDenomination() {
        return denomination;
    }
}
