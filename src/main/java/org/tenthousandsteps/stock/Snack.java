package org.tenthousandsteps.stock;

public enum Snack {

    CRISPS(75),
    MARS(70),
    COKE(100),
    EUGENIA(50),
    WATER(85);

    private final int price;

    Snack(int price) {
        this.price = price;
    }

    public int getPrice() {
        return price;
    }
}
