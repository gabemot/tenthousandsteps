package org.tenthousandsteps.stock;

import java.util.HashMap;
import java.util.Map;

public class Inventory<T> {

    private final Map<T, Integer> itemsInventory = new HashMap<>();

    public int getQuantity(T stuff) {
        Integer value = itemsInventory.get(stuff);
        return value == null ? 0 : value;
    }

    public void add(T stuff) {
        int count = itemsInventory.get(stuff);
        itemsInventory.put(stuff, count + 1);
    }

    public void deduct(T stuff) {
        if (hasItem(stuff)) {
            int count = itemsInventory.get(stuff);
            itemsInventory.put(stuff, count - 1);
        }
    }

    public boolean hasItem(T stuff) {
        return getQuantity(stuff) > 0;
    }

    public void clear() {
        itemsInventory.clear();
    }

    public void put(T stuff, int quantity) {
        itemsInventory.put(stuff, quantity);
    }
}
