package org.tenthousandsteps;

import org.tenthousandsteps.stock.Coin;
import org.tenthousandsteps.stock.Inventory;
import org.tenthousandsteps.stock.Snack;
import org.tenthousandsteps.util.Pair;

import java.util.List;

public interface SnackMachine {
    int selectSnackAndGetPrice(Snack snack);
    void insertCoin(Coin coin);
    List<Coin> refund();

    Pair<Snack, List<Coin>> collectSnackAndChange();
    void reset();

    Inventory<Snack> getSnackInventory();
    int getCurrentBalance();
}
