﻿private List getChange(long changeAmount) throws NotSufficientChangeException {
    List changes = Collections.emptyList();
    int CHANGE_PENNY = 0;
    int CHANGE_NICKEL = 0;
    int CHANGE_DIME = 0;
    int CHANGE_QUARTER = 0;
    int CHANGE_DOLLAR = 0;
    if (changeAmount > 0) {
        changes = new ArrayList();
        long remaining = changeAmount;
        while (remaining > 0) {
            if (remaining >= Coin.DOLLAR.getCents() &&
                coinsInventory.isInStock(Coin.DOLLAR) && CHANGE_DOLLAR < coinsInventory.getQuantity(Coin.DOLLAR)) {
                changes.add(Coin.DOLLAR);
                remaining = remaining - Coin.DOLLAR.getCents();
                CHANGE_DOLLAR++;
                continue;
            } else if (remaining >= Coin.QUARTER.getCents() &&
                coinsInventory.isInStock(Coin.QUARTER) && CHANGE_QUARTER < coinsInventory.getQuantity(Coin.QUARTER)) {
                changes.add(Coin.QUARTER);
                remaining = remaining - Coin.QUARTER.getCents();
                CHANGE_QUARTER++;
                continue;
            } else if (remaining >= Coin.DIME.getCents() &&
                coinsInventory.isInStock(Coin.DIME) && CHANGE_DIME < coinsInventory.getQuantity(Coin.DIME)) {
                changes.add(Coin.DIME);
                remaining = remaining - Coin.DIME.getCents();
                CHANGE_DIME++;
                continue;
            } else if (remaining >= Coin.NICKEL.getCents() &&
                coinsInventory.isInStock(Coin.NICKEL) && CHANGE_NICKEL < coinsInventory.getQuantity(Coin.NICKEL)) {
                changes.add(Coin.NICKEL);
                remaining = remaining - Coin.NICKEL.getCents();
                CHANGE_NICKEL++;
                continue;
            } else if (remaining >= Coin.PENNY.getCents() &&
                coinsInventory.isInStock(Coin.PENNY) && CHANGE_PENNY < coinsInventory.getQuantity(Coin.PENNY)) {
                changes.add(Coin.PENNY);
                remaining = remaining - Coin.PENNY.getCents();
                CHANGE_PENNY++;
                continue;
            } else {
                throw new NotSufficientChangeException(
                    "Not Sufficient Change, Please try another product");
            }
        }

    }
    return changes;
}